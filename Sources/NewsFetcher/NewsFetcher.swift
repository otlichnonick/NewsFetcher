//
//  NewsService.swift
//  FourthHomeworkApp
//
//  Created by Anton Agafonov on 22.07.2022.
//

import Foundation
import Combine
import NewsModel
import APIPackage

public struct NewsFetcher: APIManager {
    public var baseURL: String = "https://api.thenewsapi.com/v1/news/"
    private let presenter: APIPresenter = .init()
    
    public init() {}
    
    public func getAllNews(queryParams: [String: String], handler: @escaping (Result<NewsModel, String>) -> Void) {
        presenter.baseRequest(publisher: fetch(endpoint: API.getAllNews(queryParams)), handler: handler)
    }
    
    public func getTopNews(queryParams: [String: String], handler: @escaping (Result<NewsModel, String>) -> Void) {
        presenter.baseRequest(publisher: fetch(endpoint: API.getTopNews(queryParams)), handler: handler)
    }
    
    public func getSelectedNews(with uuid: String, and queryParams: [String: String], handler: @escaping (Result<DataModel, String>) -> Void) {
        presenter.baseRequest(publisher: fetch(endpoint: API.getDetailNews(uuid, queryParams)), handler: handler)
    }
}

extension NewsFetcher {
    enum API {
        case getAllNews([String: String])
        case getTopNews([String: String])
        case getDetailNews(String, [String: String])
    }
}

extension NewsFetcher.API: APICall {
    var path: String {
        switch self {
        case .getAllNews(let queryParams):
            return URLBuilder.buildQuery(path: "all", queryParams: queryParams)
        case .getTopNews(let queryParams):
            return URLBuilder.buildQuery(path: "top", queryParams: queryParams)
        case .getDetailNews(let uuid, let queryParams):
            return URLBuilder.buildQuery(path: "uuid/\(uuid)", queryParams: queryParams)
        }
    }
    
    var method: HTTPMethod {
        return .GET
    }
    
    var headers: HTTPHeaders? {
        return nil
    }
}
